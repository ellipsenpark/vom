;; some toy examples on how to use the vom library ...
(in-package :vom)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; EX. 1 - a learn a binary distribution ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defparameter *binary-pfa* (learn-pfa '(0 1) 4 0.01 30 '(1 0 0 0 1 1 0 0 1 0 0 1 0 1 0 0 0 1 1 0 0 1 1 0 1 0 1 0 0 0 0 1 1 1 0 1 0 0 0 1 1 1 1 0 0 1 1 1 0 1 0 1)))

;; set the starting state
(pfa-set-current-state *binary-pfa* '(0))

;; generate the next symbol ...
(next-transition *binary-pfa*)
(pfa-history *binary-pfa*)

;; convert (inflate) to suffix tree pfa
(pfa->st-pfa *binary-pfa*)

;; now the growth operation works (adds a new symbol and connects it based on the PFA's history)
(grow-st-pfa *binary-pfa* 2 1 0) 

;; print as svg (file will be named 'binary-pfa.dot.svg')
(pfa->svg *binary-pfa* "binary-pfa")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; EX. 2 - a learn a dna distribution ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; another one, with a different alphabet ... 
(defparameter *dna-pfa* (learn-pfa '(a g c t) 2 0.01 30 '(a g g a c t g t c t a a a c t a a g g t c c t c c a t t g g a g c a t t a a c a t c a t c t c t c a t g a g c a t g a g a g c a g c a g c a g c a g c a g c a g c a g c a g g g c a t g a g c a t c a g a g g a g g a t g a t a g a t t a t a g a c t)))

o
(defparameter *dna-pst* (learn-pst '(a g c t) 2 0.01 30 '(a g g a c t g t c t a a a c t a a g g t c c t c c a t t g g a g c a t t a a c a t c a t c t c t c a t g a g c a t g a g a g c a g c a g c a g c a g c a g c a g c a g c a g g g c a t g a g c a t c a g a g g a g g a t g a t a g a t t a t a g a c t)))

;; same as above ... 
(pfa-set-current-state *dna-pfa* '(a))
(next-transition *dna-pfa*)

;; graphical output ...
(pfa->svg *dna-pfa* "dna-pfa" )
(pst->svg *dna-pst* "dna-pst" )

(remove-duplicates  :test 'equal)

;; convert and grow ...
(pfa->st-pfa *dna-pfa*)
(grow-st-pfa *dna-pfa* 2 2 0 )


;;;;;;;;;;;;;;;;;;;;;;;
;; EX. 3 - toki pona ;;
;;;;;;;;;;;;;;;;;;;;;;;

;; toki pona - this one might take a while ... 
(time 
 (defparameter *pona-pfa* (learn-pfa '(a e i o u j k l m n p s t w) 3 0.01 150
				     '(m a m a p i m i m u t e o s i n a l o n s e w i k o n
				       n i m i s i n a l i s e w i 
				       m a s i n a o k a m a
				       j a n o p a l i e w i l e s i n a l o n s e w i k o n e n l o n m a
				       o p a n a e m o k u p i t e n p o s u n o n i t a w a m i m u t e
				       o w e k a e p a l i i k e m i s a m a l a m i w e k a e p a l i i k e p i j a n a n t e
				       o l a w a a l a e m i t a w a i k e
				       o l a w a e m i t a n i k e
				       t e n p o a l i l a s i n a j o e m a e w a w a e p o n a 
				       a m e n))))
;; same as above ... 
(pfa-set-current-state *pona-pfa* '(a))
(next-transition *pona-pfa*)
(pfa->svg *pona-pfa* "pona-pfa")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; EX. 4 - infer from a small set of rules ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; It's also possible to infer a suffix tree pfa from a set of rules as seen below. 

;; Rule format is: <source state> <emitted symbol> <symbol probability>

;; Ideally, you should provide a complete first-order distribution,
;; and for each source state you need to provide a distribution for
;; the whole alphabet.

(defparameter *a1*
  (infer-st-pfa
   '((1) 2 1.0)
   '((2) 3 1.0)
   '((3) 4 1.0)
   '((4) 5 1.0)
   '((5) 1 1.0)))

(defparameter *b1*
  (infer-st-pfa
   '((1) 2 1.0)
   '((2) 3 1.0)
   '((3) 4 1.0)
   '((4) 5 1.0)
   '((5) 6 1.0)
   '((6) 7 1.0)
   '((7) 1 1.0)))

(defparameter *a2*
  (infer-naive-pfa
   '((1) 2 100)
   '((2) 3 100)
   '((3) 4 100)
   '((4) 5 100)
   '((5) 1 100)))

(defparameter *b2*
  (infer-naive-pfa
   '((1) 2 100)
   '((2) 3 100)
   '((3) 4 100)
   '((4) 5 100)
   '((5) 6 100)
   '((6) 7 100)
   '((7) 1 100)))

(graph->svg *b2* "lala" :renderer 'neato)

(pfa-set-current-state *a1* '(1))

(next-transition *a1*)
(next-transition *b2*)

(transfer-state *a1* *b2*)

;; this state doesn't add new information but
;; might be helpful if you want to manipulate the
;; ST-PFA later on.
(st-pfa-inflate-state *a* '(1 1 1))

;; same as above ... 

(pfa-current-state *a*)

;; no need to convert here, the infer function already
;; creates a suffix tree pfa
(grow-st-pfa *a* 2 3)

(pfa-history *a*)

(pfa->svg *a* "inferred-pfa")

;;;;;;;;;;;;;;;;;;;;;;;
;; EX. 5 - naive-pfa ;;
;;;;;;;;;;;;;;;;;;;;;;;

;; infer a naive pfa (more compact internal representation)
(defparameter *g1*
  (infer-naive-pfa
   '((1) 2 25)
   '((1) 1 25) 
   '((2) 1 20)
   '((1) 5 50)
   '((2) 3 40)
   '((2) 5 40)
   '((3) 4 50)
   '((3) 5 50)
   '((4) 1 50)
   '((1 5) 3 100)
   '((4) 5 50)
   '((5) 6 50)
   '((5) 7 50)
   '((6) 4 100)
   '((7) 3 100)))

(defparameter *g2*
  (infer-naive-pfa
   '((1) 2 100)
   '((2) 3 100)
   '((3) 4 100)
   '((4) 1 100)))

(defparameter *g3*
  (infer-naive-pfa
   '((1) 2 100)
   '((2) 3 100)
   '((3) 4 100)
   '((4) 5 100)
   '((5) 1 100)))

;; transfer state
(transfer-state *g2* *g3*)

;; get the transition
(next-transition *g3*)

;; you can remove nodes
(remove-node *g* 5)

(remove-node *g* '(1 5))

(remove-node *g* 1)

;; shrink it again (remove nodes based on history)
(prune *g*)

;; there's different methods to grow ... 
(grow-old *g* :higher-order 3 :rnd 30)
(grow-triloop *g3* :higher-order 0 :rnd 0)

;; output it ...
(graph->svg *g* "gala8" :renderer 'dot)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; EX. 6 - adj-list pfa (another representation) ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defparameter *d* (learn-adj-list-pfa '(a g c t) 2 0.01 30 '(a g g a c t g t c t a a a c t a a g g t c c t c c a t t g g a g c a t t a a c a t c a t c t c t c a t g a g c a t g a g a g c a g c a g c a g c a g c a g c a g c a g c a g g g c a t g a g c a t c a g a g g a g g a t g a t a g a t t a t a g a c t)))

(defparameter *g*
  (infer-naive-pfa
   '((a) b 100)  
   '((b) c 100)
   '((c) d 100)
   '((d) a 100)
   ))


(defparameter *g*
  (infer-naive-pfa
   '((a) b 20)
   '((a) c 20)
   '((a) d 60)   

   '((b) a 20)
   '((b) c 20)
   '((b) d 60)

   '((c) a 20)
   '((c) b 20)
   '((c) d 60)
   
   '((d) a 20)
   '((d a d a) c 100)
   '((d) c 20)
   '((d) b 60)
   ))

(next-transition *g*)

(all-transitions-between *g* 'a 'c)

(alexandria::hash-table-plist (children *g*))

(gethash '(c) (children *g*))

(get-history-transitions *g*)

(discourage-pfa *g* 0.1)
(history *g* )

(defparameter *g*
  (infer-adj-list-pfa
   '((a) b 0.20)
   '((a) c 0.20)
   '((a) d 0.60)   

   '((b) a 0.20)
   '((b) c 0.20)
   '((b) d 0.60)

   '((c) a 0.20)
   '((c) b 0.20)
   '((c) d 0.60)
   
   '((d) a 0.20)
   '((d a d a) c 1.00)
   '((d) c 0.20)
   '((d) b 0.60)))

(sharpen-pfa *g* 0.4)
(blur-pfa *g* 0.8)

(adj-list-pfa->svg *g* "lupus")
(graph->svg *g* "lupus")

(current-node *g*)
(next-transition *g*)

;; grow in a similar fashion ...
(grow-old *g*)

(grow-loop *g*)

;; get the pst from this one 
(pst->svg (pst-root *g*) "lopopo_pst")

;; insert a new rule at will ...
(gethash '(a) (children *g*))

(insert-rule *g* '((c b c b) d 1.0))

(add-node (pst-root *g*) '(c b c b))

(adj-list-pfa-add-prefix-suffix *g* '(c b c b))

(insert-rule *g* '((c b c b) d 1.0))

(randomize-edges *g* 70)

;; visualize ...
(adj-list-pfa->svg *g* "before")
(adj-list-pfa->svg *g* "after")







