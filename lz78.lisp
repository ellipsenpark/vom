;; generate lz78 dictionaries ... 
(in-package :vom)

(defun lz78-generate-dictionary (phrase)
  "generate a dictionary of non-overlapping adjacent phrases for the lz78 algorithm"
  (labels ((add-phrase (dict word phrase)
	     (cond ((not word) dict) ;; no more word to process		   
		   ((not phrase) (append dict (list word))) ;; last word
		   ((member word dict :test #'equal) ;; word found
		    (add-phrase dict
				(append word (list (car phrase)))
				(cdr phrase)))		    
		   (t (add-phrase (append dict (list word)) ;; word not found 
				  (list (car phrase))
				  (cdr phrase))))))
    (add-phrase '() (list (car phrase)) (cdr phrase))))

;; labels are implicit through the hashtable keys ...
(defstruct lz78-trie-node  
  (count 0 :type integer)
  (children nil :type hash-table))

(defun lz78-expand-trie (node phrase alphabet)
  (when (eql (lz78-trie-node-count node) 1)
    (loop for char in alphabet
       do (setf (gethash char (lz78-trie-node-children node))
		(make-lz78-trie-node :count 1 :children (make-hash-table)))))
  ;; expand
  (when (> (length phrase) 0)
    (lz78-expand-trie
     (gethash (car phrase) (lz78-trie-node-children node))
     (cdr phrase)
     alphabet))
  ;; count 
  (setf (lz78-trie-node-count node)
	(loop for child being the hash-values of (lz78-trie-node-children node)
	   summing (lz78-trie-node-count child))))

(defun lz78-generate-trie (dict alphabet)
  "generate a prediction trie from an lz78 dictionary"
  (let* ((root (make-lz78-trie-node :count 1 :children (make-hash-table))))
    (loop for phrase in dict
       do (lz78-expand-trie root phrase alphabet))
    ;; return root
    root))

(defun lz78-follow-trie (root node phrase)
  (cond ((and (not phrase) (eql (lz78-trie-node-count node) 1)) root)	
	((not phrase) node)
	((eql (lz78-trie-node-count node) 1)
	 (lz78-follow-trie root root (cdr phrase)))
	(t (lz78-follow-trie root (gethash (car phrase)
					 (lz78-trie-node-children node))
			   (cdr phrase)))))

(defun lz78-probability (symbol context trie)
  "calculate the probability for a certain symbol to follow a context given a trie"
  (let ((context-node (lz78-follow-trie trie trie context)))
    (float (/ (lz78-trie-node-count
	       (gethash symbol (lz78-trie-node-children context-node)))
	      (lz78-trie-node-count context-node)))))




