(in-package :vom)

(defun remove-all (items seq &key (test 'equal))
  (remove-if #'(lambda (i) (member i items :test test)) seq))

(defun dest-equals (a b)
  (equal (cdr a) (cdr b)))

(defun trans-equals (a b)
  (or
   (equal (cadr a) (last (cadr b)))
   (equal (cdr a) (cdr b))))

(defstruct query-result
  (last-state '() :type list)
  (current-state '() :type list)
  (symbol))

(defstruct growth-result
  (added-symbol)
  (template-symbol)  
  (removed-states '() :type list)
  (added-transitions '() :type list)
  (removed-transitions '() :type list))

