;; Probablistic Finite/Suffix Automata, following Ron, Singer and Tishby ...
(in-package :vom)

;; the basic probablistic finite automaton structures ... 
(defclass pfa-state ()
  ((label :accessor pfa-state-label :initarg :label)   
   (next-sym-prob :accessor pfa-state-next-sym-prob :initform (make-hash-table))
   ;; keep track of choice list while constructing transitions,
   ;; so we don't have to build it every time we need to make a choice ... 
   (choice-list :accessor pfa-state-choice-list :initform (list))
   (transitions :accessor pfa-state-transitions :initform (make-hash-table))))

(defclass pfa ()
  ((current-state :accessor pfa-current-state :initform nil)
   (states :accessor pfa-states :initform (make-hash-table :test #'equal))
   (alphabet :accessor alphabet :initform '())
   (history-length :accessor pfa-history-length :initform 100)
   (history :accessor pfa-history :initform (list))))

(defmethod get-all-transitions ((p pfa))
  (loop for state being the hash-values of (pfa-states p)
        append (loop for next being the hash-values of (pfa-state-transitions state)
                     collect (list (pfa-state-label state) (pfa-state-label next)))))

(defmethod next-transition ((p pfa) &key)
  (let* ((current-state (pfa-current-state p))
	 (next-symbol (nth (random (length (pfa-state-choice-list current-state)))
			   (pfa-state-choice-list current-state))))    
    (setf (pfa-current-state p)
	  (gethash next-symbol (pfa-state-transitions current-state)))
    (setf (pfa-history p)
	  (nconc (pfa-history p) (list next-symbol)))
    (when (> (length (pfa-history p)) (pfa-history-length p))
      (setf (pfa-history p) (cdr (pfa-history p))))
    (make-query-result :last-state (pfa-state-label current-state) :current-state (pfa-state-label (pfa-current-state p)) :symbol next-symbol)))

(defmethod pfa-get-state ((p pfa) label)
  (gethash label (pfa-states p)))

(defmethod pfa-add-state ((p pfa) label &key (overwrite nil))
  (if (or overwrite (not (pfa-get-state p label)))
    (setf (gethash label (pfa-states p))
	  (make-instance 'pfa-state :label label))
    (pfa-get-state p label)))

(defmethod pfa-state-rebuild-choice-list ((p pfa-state))
  (let ((new-choice-list '()))
    (loop for sym being the hash-keys of (pfa-state-next-sym-prob p)
       do (nconc new-choice-list (loop for i from 0 to (* (gethash sym (pfa-state-next-sym-prob p)) 100) collect sym)))
    (setf (pfa-state-choice-list p) new-choice-list))) ;; reset
    
(defmethod pfa-has-transition ((p pfa) src-state-label dest-state-label &key)
  ;; otherwise there's nothing to do ...
  (when (and (pfa-get-state p src-state-label) (pfa-get-state p dest-state-label))
    (let ((trans-symbol (car (reverse dest-state-label))))
      (if (gethash trans-symbol
		   (pfa-state-transitions (pfa-get-state p src-state-label))) t))))

;; THIS WON'T MODIFY THE EMISSION PROBABILITY FOR A SYMBOL !! (for now ...)
;; (later i might add check to see if the replacement is valid ...)
(defmethod pfa-replace-transition ((p pfa) src old-dest new-dest)
  (when (pfa-has-transition p src old-dest)
    (let ((trans-symbol (car (reverse old-dest))))
      (setf (gethash trans-symbol
		     (pfa-state-transitions (pfa-get-state p src)))
	    (gethash new-dest (pfa-states p))))))

(defmethod pfa-add-transition ((p pfa) src dest symbol &key (prob 0) (build-choice-list t))
  ;;(format t "add: ~D -> ~D p ~D sym ~D~%" src dest prob symbol)
  (when build-choice-list (setf (pfa-state-choice-list (gethash src (pfa-states p)))
				(nconc (delete symbol (pfa-state-choice-list
						       (gethash src (pfa-states p))))
				       (loop for i from 0 to (* prob 100) collect symbol))))
  (setf (gethash symbol (pfa-state-next-sym-prob
		    (gethash src (pfa-states p)))) prob)
  (setf (gethash symbol (pfa-state-transitions
		    (gethash src (pfa-states p))))
	(gethash dest (pfa-states p))))

(defmethod pfa-set-current-state ((p pfa) label &key)
  (setf (pfa-current-state p)
	(gethash label (pfa-states p))))

(defmethod pfa->dot ((p pfa) output)
  (format output "digraph{~%")  
  (loop for state being the hash-values of (pfa-states p)
     using (hash-key key)
     do (progn
	  (format output "~a[label=\"~{~a~^, ~}\"]~%"
		  (sxhash key)
		  key)
	  (loop for symbol being the hash-keys of (pfa-state-next-sym-prob state)
	       when (gethash symbol (pfa-state-transitions state))
	       do (format output
			  "~a->~a[weight=~a, penwidth=~a, rank=same, arrowsize=1.0, label=~a]~%"			  
			  (sxhash key)
			  (sxhash (pfa-state-label (gethash symbol (pfa-state-transitions state))))
			  (float (gethash symbol (pfa-state-next-sym-prob state)))
			  (float (gethash symbol (pfa-state-next-sym-prob state)))
			  symbol))))
  (format output "~%}"))

(defmethod pfa->svg ((p pfa) basename &key)
  (with-open-file (out-stream (concatenate 'string basename ".dot")
			      :direction :output :if-exists :supersede)
    (pfa->dot p out-stream))
  (sb-ext:run-program "/usr/bin/dot" (list "-T" "svg" "-O"
					   (concatenate 'string basename ".dot")
					   "-Goverlap=scalexy -Gnodesep=0.6 -Gstart=0.5")))

(defun pst->psa (pst)
  "convert pst to pfa if pst has 'star' property ... "
  ;; tbd
  )

(defun find-longest-suffix-state (pst-root label)
  (let ((label-rev (reverse label)))
    (labels ((find-node (cur-node rev-label)	       
	       (if rev-label ;; if not, we've reached a leaf ...
		   (let ((cur-child (gethash (car rev-label)
					     (pst-node-children cur-node))))
		     (if (not cur-child)
			 cur-node
			 (find-node cur-child (cdr rev-label))))
		   cur-node)))
      (find-node pst-root label-rev))))

(defun pst-nostar->pfa (extended-pst-root alphabet)
  "convert pst to pfa if pst doesn't have 'star' property ... "
  ;; leaves and all the prefixes are the states of the pfa,
  ;; meaning all the states of the pst, basically ... 
  (let ((new-pfa (make-instance 'pfa)))
    ;; add states
    (labels ((copy-state (node)
	       (pfa-add-state new-pfa (pst-node-label node))
	       (loop for child being the hash-values of (pst-node-children node)
		     do (copy-state child))))
      ;; copy them over ... 
      (copy-state extended-pst-root))
    ;; add transitions
    (loop for state being the hash-values of (pfa-states new-pfa)
          do (let* ((label (pfa-state-label state))
		    (pst-state (find-longest-suffix-state extended-pst-root label)))
	       (loop for symbol in alphabet
	             do (let ((longest-suffix-pst-state
			        (find-longest-suffix-state
			         extended-pst-root (append label (list symbol)))))
		          (pfa-add-transition new-pfa
					      label
					      (pst-node-label longest-suffix-pst-state)
					      symbol
					      :prob (gethash symbol
						             (pst-node-child-prob
							      pst-state)))))))
    new-pfa))

(defun pst->pfa (pst alphabet)
  (if (star-property pst alphabet)
      (pst->psa pst)
      (pst-nostar->pfa pst alphabet)))

(defun learn-pfa (alphabet bound epsilon n sample-string)
  "learn a pfa from a given sample"
  (let ((new-pst (learn-pst alphabet bound epsilon n sample-string)))
    (pst->pfa (extend-pst new-pst (collect-pst-leaves new-pst) alphabet) alphabet)))
