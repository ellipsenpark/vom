;; Probablistic Suffix Trees, following Ron, Singer and Tishby ...
(in-package :vom)

;; pst learning methods

;; reference version, conses new lists by using subseq ...
(defun chi (sample-string token j)  
  "chi function following Ron, Singer and Tishby"
  (declare (optimize (speed 3)))
  (declare (type list sample-string token) (type fixnum j))
  (if (equal (subseq sample-string (- j (length token)) j) token) 1 0))

;; optimized, cons-free version ...
(defun chi2 (sample-string token j)
  "chi function following Ron, Singer and Tishby, cons-free"
  (declare (optimize (speed 3) (safety 0)))
  (declare (type list sample-string token) (type fixnum j))
  (loop with l fixnum = (length token)
	with b fixnum = (- j l)
	with a fixnum = (- l 1)
	for tidx fixnum from 0 to a
	for sidx fixnum = (+ b tidx)
	do (when (not (eq (nth tidx token)
		      (nth sidx sample-string)))
	 ;; return 0 immediately when non-match being found
	 (return-from chi2 0))) 
  ;; return 1 if all symbols match ...
  1)

;; same with a symbol appended, to avoid having to cons a new token ...
(defun chi2-with-symbol (sample-string token symbol j)
  "chi function following Ron, Singer and Tishby, cons-free, with an appended symbol"
  (declare (optimize (speed 3) (safety 0)))
  (declare (type list sample-string token) (type fixnum j))
  (loop with l fixnum = (+ (length token) 1)
	with b fixnum = (- j l)
	with a fixnum = (- l 2)
	for tidx fixnum from 0 to a
	for sidx fixnum = (+ b tidx)
	do (if (not (eq (nth tidx token)
			  (nth sidx sample-string)))
	     ;; return 0 immediately when non-match being found
	     (return-from chi2-with-symbol 0))
	finally (if (not (eq symbol (nth (- j 1) sample-string)))
		    ;; return 0 immediately when non-match being found
		    (return-from chi2-with-symbol 0))) 
  ;; return 1 if all symbols match ...
  1)

;; probability functions

(defun empirical-probability (sample-string m token bound)
  "determine empirical probablity of token given sample-string of length m"
  (declare (optimize (speed 3)))
  (/ (loop for j from (min bound (length token)) to (- m 1)
	   sum (chi2 sample-string token j)) (- m bound)))

;; reference version
(defun empirical-probability-sym (sample-string m token symbol bound)
  "determine empirical probablity of symbol, given token, sample-string of length m"
  (declare (optimize (speed 3)))
  (if token
      (let* ((ts (append token (list symbol)))
	     (p-a (loop for j from bound to (- m 1)
		     sum (chi2 sample-string ts (+ j 1))))
	     (p-b (loop for j from bound to (- m 1)
		     sum (chi2 sample-string token j))))
	(if (and (eq p-a 0) (eq p-b 0)) 0 (/ p-a p-b)))
      (empirical-probability sample-string m (list symbol) bound)))

;; cons-free, optimized version ... 
(defun empirical-probability-sym2 (sample-string m token symbol bound)
  "determine empirical probablity of symbol, given token, sample-string of length m"
  (declare (optimize (speed 3)))
  (declare (type list sample-string token) (type fixnum m bound))
  (if token
      (let* ((p-b (loop for j fixnum from bound to (- m 1)
			sum (chi2 sample-string token j)))	     
	     (p-a (loop for j fixnum from bound to (- m 1)
			sum (chi2-with-symbol sample-string token symbol (+ j 1)))))
	(if (and (eq p-a 0) (eq p-b 0)) 0 (/ p-a p-b)))
      (empirical-probability sample-string m (list symbol) bound)))

;; prediction suffix trees, an intermediate step in the learning process ...
(defstruct pst-node  
  (label nil :type list)
  (child-prob nil :type hash-table)
  (children nil :type hash-table))

(defun add-node (root label)
  "add node to a pst, eventually adding nodes on the path ..."
  (let ((label-rev (reverse label))
        (added-nodes nil))
    (labels ((find-leaf-and-add (cur-node rev-label n-label)	       
	       (when rev-label		   		 
		 (let ((cur-child (gethash (car rev-label)
					   (pst-node-children cur-node))))
		   (push (car rev-label) n-label)
		   (when (not cur-child)
                     (push (copy-list n-label) added-nodes)
		     (setf (gethash (car rev-label) (pst-node-children cur-node))
                           (make-pst-node :label n-label
					  :child-prob (make-hash-table)
					  :children (make-hash-table))))
		   (find-leaf-and-add (gethash (car rev-label)
					       (pst-node-children cur-node))
				      (cdr rev-label) n-label)))))
      (find-leaf-and-add root label-rev (list)))
    added-nodes
    ))

;; not really sure how to determine n (max number of psa states) ... 
(defun learn-pst (alphabet bound epsilon n sample-string)
  "learn a pst from a sample string, bounded by length bound"
  ;;(format t "SAMPLE: ~a~%" sample-string)
  (let* ((m (length sample-string))
	 (la (length alphabet))
	 ;; values according to paper ... 
	 (epsilon2 (/ epsilon (* 48.0 bound)))
	 (gamma-min (/ epsilon2 la))
	 (epsilon0 (/ epsilon (* 2 n bound (log (/ 1 gamma-min)))))
	 (epsilon1 (/ epsilon2 (* 8 n epsilon0 gamma-min)))
	 ;; init tokens with single symbols
	 (tokens (loop for c in alphabet if		      
		      (>= (empirical-probability sample-string m (list c) bound)
			  (* epsilon0 (- 1 epsilon1))) 
		    collect (list c)))
	 ;; the pst root node ...
	 (tree (make-pst-node :label nil
			      :child-prob (make-hash-table)
			      :children (make-hash-table))))
    ;;(format t "e ~D bound ~D n ~D e0 ~D e1 ~D e2 ~D gmin ~D ~%" epsilon bound n epsilon0 epsilon1 epsilon2 gamma-min)
    ;; build (scaffold) initial prediction suffix tree ... 
    (loop while tokens
       do (let ((s (car tokens))) ;; pick a token
	    (setf tokens (delete s tokens)) ;; remove picked token
	    ;; check if we need to add this token to the tree ... 
	    (loop for symbol in alphabet
	       do (let ((symp (empirical-probability-sym2
			       sample-string m s symbol bound))
                        (symp-next (empirical-probability-sym2
				    sample-string m (cdr s) symbol bound)))
                    ;;(format t "CHECK ~a ~a ~D ~D ~D ~D ~D ~%" s (cdr s) symbol symp symp-next m bound )
		    (when (and (>= symp (* (+ 1 epsilon2) gamma-min))
                               (> symp-next 0.0)
                               (> (/ symp symp-next)
				  (+ 1 (* 3 epsilon2)))) ;; cdr == suffix
                      ;;(format t "ADD: ~a ~D symp ~D sympth ~D sympsuf ~D sympdufth ~D  ~%" s symbol symp (float (* (+ 1 epsilon2) gamma-min)) (float (/ symp symp-next)) (float (+ 1 (* 3 epsilon2))))
		      (add-node tree s)
		      ;; finish after checking ... 
		      (loop-finish))))
	    ;; check if we need to add tokens to the list ... 
	    (when (< (length s) bound)
	      (loop for symbol in alphabet
		    do (let ((newtok (append (list symbol) s)))
			 ;; IDEA - make a function empirical-probabilityPLUSSYM,
			 ;; so we only need to cons when we actually need the
			 ;; new token ...
			 (when (> (empirical-probability
				    sample-string m newtok bound)
				   (max 0.0 (* (- 1 epsilon1) epsilon0)) )                           
                           ;;(format t "POSSIBLE TOK ~D ~D ~a~%" (float (empirical-probability sample-string m newtok bound)) (float (* (- 1 epsilon1) epsilon0)) newtok)
			   (setf tokens (nconc tokens (list newtok)))))))))
    ;; add missing internal nodes to tree (copy) ...
    (labels ((traverse-complete (node) ;; depth-first ... 
	       (loop for child being the hash-values of (pst-node-children node)
		  do (traverse-complete child))
	       ;; if the node is not a leaf, but incomplete ...
	       (when (and (> (hash-table-count (pst-node-children node)) 0)
			  (< (hash-table-count (pst-node-children node)) la))
		 (let ((remainder (copy-list alphabet)))                   
                   (loop for key being the hash-keys of (pst-node-children node)
			 do (setf remainder (delete key remainder)))
		   (loop for symbol in remainder
		         do (setf (gethash symbol (pst-node-children node))
			          (make-pst-node :label
					         (append (list symbol)
						         (pst-node-label node))
					         :child-prob (make-hash-table)
					         :children (make-hash-table))))))))
      ;;(format t "travom")
      (traverse-complete tree))
    ;; now, fill the probability functions ...
    ;; it might be more efficient to combine this with the one above ??
    (labels ((complete-gamma (parent node) ;; depth-first ...
	       (if parent
		   (loop for symbol in alphabet
		      do (setf (gethash symbol (pst-node-child-prob node))
			       (empirical-probability-sym2
				sample-string m (pst-node-label parent)
				symbol bound)))
		   (loop for symbol in alphabet
		      do (setf (gethash symbol (pst-node-child-prob node))
			       (+ (* (empirical-probability-sym2 sample-string m
								nil symbol bound)
				     (- 1 (* la gamma-min)))
				  gamma-min))))
	       (loop for child being the hash-values of (pst-node-children node)
		  do (complete-gamma node child))))
      (complete-gamma nil tree))
    tree))

(defun extend-if-not-present (root label)
  "add this label if node is not present, and assign the gamma function of its parent node"
  (let ((label-rev (reverse label)))
    (labels ((find-leaf-and-add (cur-node rev-label n-label)	       
	       (when rev-label ;; if not, we've reached a leaf ...		   		   
		 (let ((cur-child (gethash (car rev-label)
					   (pst-node-children cur-node))))
		   (push (car rev-label) n-label)
		   (when (not cur-child)
		     (let ((new-node (make-pst-node :label n-label
						    :child-prob (make-hash-table)
						    :children (make-hash-table))))
		       ;; deepcopy parent prob map ...
		       (loop for key being the hash-keys of
			    (pst-node-child-prob cur-node)
			  do (setf (gethash key (pst-node-child-prob new-node))
				   (gethash key (pst-node-child-prob cur-node))))
		       (setf (gethash (car rev-label)
				      (pst-node-children cur-node)) new-node)))
		   (find-leaf-and-add (gethash (car rev-label)
					       (pst-node-children cur-node))
				      (cdr rev-label) n-label)))))
      (find-leaf-and-add root label-rev (list)))))

(defun collect-pst-leaves (root)
  (let ((leaves (list)))
    ;; collect leaves ... 
    (labels ((collect-leaves (node)	       
	       (if (equal (hash-table-size (pst-node-children node)) 0)
		   (push node leaves)
		   (loop for child being the hash-values of (pst-node-children node)
		      do (collect-leaves child)))))
      (collect-leaves root))
    leaves))

(defun extend-pst (root leaves alphabet)
  "extend pst so the transition function of the subsequent pfa will be defined"
  (loop for leave in leaves
     do (loop for symbol in alphabet
	   do (extend-if-not-present root (append (pst-node-label leave) (list symbol)))))
    root)

(defun pst-prob (pst seq)
  "the probablity of a sequence given the pst"
  ;;(labels ((rec-prob (node seq))))
  ;; tbd
  nil)

(defun star-property (pst alphabet)
  "the 'star' property as defined in the paper"
  ;; tbd
  nil)

(defun find-longest-suffix-label (root label)
  (pst-node-label (find-longest-suffix-state root label)))

(defun get-states-with-symbol (root symbol)
  (labels ((collect-states (root symbol states)             
             (if (equal (hash-table-size (pst-node-children root)) 0)
                 (if (member symbol (pst-node-label root))
                     (nconc states (list (pst-node-label root)))
                     states)
                 (if (member symbol (pst-node-label root))
                     (nconc states (list (pst-node-label root))
                            (loop for ch being the hash-values of (pst-node-children root)
                                  nconc (collect-states ch symbol states)))
                     (nconc states 
                            (loop for ch being the hash-values of (pst-node-children root)
                                  nconc (collect-states ch symbol states)))))))
    (collect-states root symbol (list)))) 

(defun get-suffix-symbol-states (pst-root symbol)
  (labels ((collect-all (root)
             (if (eq (hash-table-size (pst-node-children root)) 0)
                 (list (pst-node-label root))
                 (nconc (list (pst-node-label root))
                        (loop for child being the hash-values of (pst-node-children root)
                              nconc (collect-all child))))))    
    (if (gethash symbol (pst-node-children pst-root))
        (collect-all (gethash symbol (pst-node-children pst-root)))))) 

(defun pst->dot (root output)
  (format output "digraph{~%")  
  (let ((idx 0))
    (labels ((node->dot (node output)
	       (let ((cur idx))
	         (format output "~a[label=\"~D\"]~%"
		         idx
		         (pst-node-label node))
	         (loop for child being the hash-values of (pst-node-children node)
		       do (progn (incf idx)			    
			         (format output
				         "~a->~a[weight=1.0, penwidth=1.0, rank=same, arrowsize=1.0]~%"
				         cur idx)
			         (node->dot child output))))))
      (node->dot root output)))
  (format output "~%}"))

(defun pst->svg (root basename)
  (with-open-file (out-stream (concatenate 'string basename ".dot")
			      :direction :output :if-exists :supersede)
    (pst->dot root out-stream))
  (sb-ext:run-program "/usr/bin/dot" (list "-T" "svg" "-O"
					   (concatenate 'string basename ".dot")
					   "-Goverlap=scalexy -Gnodesep=0.6 -Gstart=0.5")))
