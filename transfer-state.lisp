(in-package :vom)

;; transfer state (if possible) between pfa
;; useful if you need state preservation somewhere
;; some might be a bit makeshift

(defmethod transfer-state ((old naive-pfa) (new naive-pfa))
  (when (member (current-node old) (alphabet new))
    (setf (current-node new) (current-node old))
    (setf (history new) (list (current-node old)))))

(defmethod transfer-state ((old adj-list-pfa) (new adj-list-pfa))
  (when (gethash (current-state old) (children new))
    (setf (current-state new) (current-state old))
    (setf (history new) (history old))))

(defmethod transfer-state ((old naive-pfa) (new adj-list-pfa))
  (when (gethash (list (current-node old)) (children new))
    (setf (current-state new) (list (current-node old)))))

(defmethod transfer-state ((old adj-list-pfa) (new naive-pfa))
  (when (member (alexandria::lastcar (current-state old)) (alphabet new))
    (setf (current-node new) (alexandria::lastcar (current-state old)))
    (setf (history new) (list (alexandria::lastcar (current-state old))))))

(defmethod transfer-state ((old pfa) (new naive-pfa))
  (when (member (alexandria::lastcar (pfa-state-label (pfa-current-state old))) (alphabet new))
    (setf (current-node new) (alexandria::lastcar (pfa-state-label (pfa-current-state old))))
    (setf (history new) (list (alexandria::lastcar (pfa-state-label (pfa-current-state old)))))))

(defmethod transfer-state ((old naive-pfa) (new pfa))
(when (gethash (list (current-node old)) (pfa-states new))
  (setf (pfa-current-state new) (gethash (list (current-node old)) (pfa-states new)))))

(defmethod transfer-state ((old pfa) (new pfa))
  (when (gethash (pfa-state-label (pfa-current-state old)) (pfa-states new))
    (setf (pfa-current-state new) (gethash (pfa-state-label (pfa-current-state old)) (pfa-states new)))))
