;; A naive implementation of a PFA that doesn't claim
;; mathematical correctness but sounds good.
;; it contains certain heuristics to remove nodes, re-balance probabilities etc ...
;; (a simplified version of the ancient, overcomplicated implementation, that is ...)
(in-package :vom)

;; some hash-table helpers
(defun delete-from (hashtable key value &key (test 'equal))
  (setf (gethash key hashtable) (delete value (gethash key hashtable) :test test)))

(defun add-to-nodup (hashtable key value &key (test 'equal))
  (setf (gethash key hashtable) (remove-duplicates (nconc (gethash key hashtable) (list value)) :test test)))

;; everything can be organized as a graph, really ...
(defclass naive-pfa ()
  ((alphabet :accessor alphabet :initform '() :initarg :alphabet)
   (current-node :accessor current-node :initform nil) 
   (parents :accessor parents :initform (make-hash-table :test 'eql))
   (childern :accessor children :initform (make-hash-table :test 'equal))
   (history :accessor history :initform '()) ;; trace is essential part of the model
   (history-length :accessor history-length :initform 9)
   (max-order :accessor max-order :initform 0)))

;; not super correct but anyway ...
(defmethod current-state ((p naive-pfa))
  (list (current-node p)))

(defmethod add-child ((p naive-pfa) src dest prob &key)
  (add-to-nodup (children p) src (cons prob dest) :test 'dest-equals))

(defmethod add-parent ((p naive-pfa) dest src &key)
  (add-to-nodup (parents p) dest src))

(defmethod rebalance-node ((p naive-pfa) source &key)
  (setf (gethash source (children p)) (rebalance (gethash source (children p)) :blur 0.35)))

(defmethod rebalance-state ((p naive-pfa) source &key)
  (rebalance-node p source))

;; INSERT
(defmethod insert-node ((p naive-pfa) label &key)
  (setf (alphabet p) (remove-duplicates (nconc (alphabet p) (list label))))
  (setf (gethash label (parents p)) '())
  (setf (gethash (list label) (children p)) '())
  (unless (current-node p)
    (setf (current-node p) label)
    (setf (history p) (nconc (history p) (list label)))))

;; simplification !!!
;; store sources in "flat" map, just match against whole trace (last to recent)
;; edge source always list ! 
;; src always formulated as list !
(defmethod insert-edge ((p naive-pfa) src dest prob &key (rebalance nil))  
  (add-child p src dest prob)
  (add-parent p dest src)
  (if rebalance (rebalance-node p src))
  (if (> (length src) (max-order p)) (setf (max-order p) (length src))))

(defmethod insert-rule ((p naive-pfa) rule &key)
  (let ((prob (if (and (floatp (caddr rule)) (<= (caddr rule) 1.0))
                  (round (* (caddr rule) 100))
                  (caddr rule))))
    (insert-edge p (car rule) (cadr rule) prob)))

(defmethod has-transition ((p naive-pfa) source dest &key (test 'dest-equals))
  (member (list 0 dest) (gethash source (children p)) :test 'dest-equals))

(defmethod get-transition ((p naive-pfa) src dest &key)
  (car (member (cons 0 dest) (gethash src (children p)) :test 'dest-equals)))

;; for now, build choice list on the fly ...
;; returns next node, and where it's coming from ...
(defmethod next-transition ((p naive-pfa) &key)
  ;; first, find highest-order parent from history ...
  ;; need to copy here, unfortunately ... 
  (let* ((parent (copy-list (loop for n from (max-order p) downto 1
                                  when (gethash (last (history p) n) (children p))
                                  return (last (history p) n))))
         (choice-list (loop for choice in (gethash parent (children p)) ;; build choice list ...
                            append (make-list (car choice) :initial-element (cdr choice))))
         (next-node (nth (random (length choice-list)) choice-list)))
    ;; update the history 
    (setf (history p) (nconc (history p) (list next-node)))
    ;; truncate history if necessary ...
    (when (> (length (history p)) (history-length p))
      (setf (history p) (cdr (history p))))
    ;; set new current node ...
    (setf (current-node p) next-node)
    ;; return transition with parent and current ...
    (make-query-result :last-state parent :current-state (list next-node) :symbol next-node)))

;; other operations -> blur (even out), sharpen (increase highest), discourage, encourage ...

;; plain deletion, no bridging ...
(defmethod purge-node ((p naive-pfa) id &key)  
  (let* ((node-id (if (listp id) id (list id)))
         (removed-transitions (nconc (mapcar #'car (remove-outgoing-edges p node-id)) (if (eql (length node-id) 1) (remove-incoming-edges p id)))))
    (when (eql (length node-id) 1)      
      (setf (history p) (delete (car node-id) (history p)))
      (setf (alphabet p) (delete (car node-id) (alphabet p)))      
      (remhash (car node-id) (parents p)))    
    (remhash node-id (children p))
    (make-growth-result :added-symbol nil :template-symbol nil :removed-states (list node-id) :added-transitions nil :removed-transitions (delete nil removed-transitions))))

(defmethod is-abandoned ((p naive-pfa) id &key)
  (let ((node-id (if (listp id) id (list id))))
    (if (eql (length node-id) 1)
        (and (not (gethash (car node-id) (parents p))) (not (gethash node-id (children p))))
        (not (gethash node-id (children p)))))) ;; higher-order don't have incoming edges ...

(defmethod remove-outgoing-edges ((p naive-pfa) id &key)
  (let ((node-id (if (listp id) id (list id))))
    (loop for child in (gethash node-id (children p))
          do (remove-edge p node-id (cdr child))
          collect (list (cons node-id (cdr child)) (car child)) ;; src - dest - old prob
          finally (setf (gethash node-id (children p)) nil))))

(defmethod remove-incoming-edges ((p naive-pfa) node-id &key)
  (loop for parent in (gethash node-id (parents p))
        do (remove-edge p parent node-id :rebalance nil)        
        collect (cons parent node-id)))

;; the removal operations are fairly heuristic so far ...
;; there's no mathematically prescribed way how to deal with it,
;; so my approach is simply to "bridge" edges from a
;; removed parent to
(defmethod remove-node ((p naive-pfa) removed-id &key (rebalance t))
  (let* ((removed-out (remove-outgoing-edges p removed-id))
         (removed-in (remove-incoming-edges p removed-id))         
         (ho-states (remove-if (lambda (e) (or (> 2 (length e)) (not (member removed-id e)))) (alexandria::hash-table-keys (children p))))
         (ho-removals (loop for state in ho-states collect (purge-node p state)))
         (removed-all (append removed-in (mapcar #'car removed-out) (loop for rem in ho-removals nconc (growth-result-removed-transitions rem))))        
         (edges-to-add (loop for ei in removed-in
                             append (loop for eo in removed-out
                                          when (and (not (equal (car ei) (list removed-id))) (not (equal (cdar eo) removed-id))) ;; avoid loops being re-added
                                          collect (list (cons (car ei) (cdar eo)) (cadr eo))))))
    ;; delete from path ...    
    (setf (history p) (delete removed-id (history p)))
    (setf (alphabet p) (delete removed-id (alphabet p)))     
    (loop for a in edges-to-add do          
          (insert-edge p (caar a) (cdar a) (cadr a) :rebalance rebalance))
    (remhash (if (listp removed-id) removed-id (list removed-id)) (children p))
    (remhash removed-id (parents p)) ;; no need to convert to list here, because there shouldn't be any list keys anyway ...    
    ;; keep our model clean, remove states that have been abandoned in the process ...    
    (let ((abandoned-states (remove-duplicates
                             (loop for rem in removed-all
                                   when (and (not (equal (car rem) (if (listp removed-id) removed-id (list removed-id))))
                                             (is-abandoned p (car rem)))
                                   collect (car rem))
                             :test 'equal)))
      (loop for st in abandoned-states do (purge-node p st))
      ;; check if the current node needs to be re-set ...
      (when (or (equal (current-node p) removed-id) (member (list (current-node p)) abandoned-states :test 'equal))
        (setf (current-node p) (car (alphabet p)))
        (setf (history p) (nconc (history p) (list (car (alphabet p))))))
      ;; communicate what has been done to the outside world ...
      (make-growth-result :added-symbol nil
                          :template-symbol nil
                          :removed-states (nconc (list (if (listp removed-id) removed-id (list removed-id))) ho-states abandoned-states)
                          :added-transitions (mapcar #'car edges-to-add) 
                          :removed-transitions removed-all))))

;; edge case - no edges ?
(defmethod remove-edge ((p naive-pfa) source destination &key (rebalance t))
  (setf (gethash source (children p)) (delete (cons 0 destination) (gethash source (children p)) :test 'dest-equals))
  (setf (gethash destination (parents p)) (delete source (gethash destination (parents p)) :test 'equal))
  (if (and rebalance (gethash source (children p))) ;; only if there's something to do ...
      (setf (gethash source (children p)) (rebalance (gethash source (children p))))))

(defmethod prune-pfa ((g naive-pfa) &key exclude node-id)
  (let* ((exclude-with-current (append exclude (list (current-node g))))
	 (reduced-history (remove-all exclude-with-current (history g)))
	 (prune-id (if node-id node-id (car (last reduced-history)))))    
    ;; otherwise the graph can't be pruned further ... 
    (when prune-id
      (remove-node g prune-id :rebalance t))))

;; GRAPHVIZ VISUALIZATION
(defmethod graph->dot ((g naive-pfa) &key (output nil))
  (format output "digraph{~%")
  (loop for label being the hash-keys of (children g) using (hash-value chs)
        do (progn
             (format output "\"~{~a~^, ~}\"[label=\"~{~a~^, ~}\"]~%" label label)             
             (loop for ch in chs
                   do (format output "\"~{~a~^, ~}\"->\"~a\"[weight=~a, penwidth=~a, rank=same, arrowsize=~a]~%"
	                      label
                              (cdr ch)
	                      (coerce (/ (car ch) 100) 'float)
	                      (coerce (/ (car ch) 10) 'float)
	                      (cond 
	                        ((> (car ch) 90) 1)
	                        ((> (car ch) 40) 0.5)
	                        ((> (car ch) 1) 0.1)
	                        (t 0))))))
  (format output "~%}"))

(defun graph->svg (graph file &key (renderer 'circo))
  (with-open-file (out-stream file :direction :output :if-exists :supersede)
    (graph->dot graph :output out-stream))
  (cond ((eq renderer 'dot)
	 (sb-ext:run-program "/usr/bin/dot" (list "-T" "svg" "-O" file)))
	((eq renderer 'neato)
	 (sb-ext:run-program "/usr/bin/neato" (list "-T" "svg" "-O"
						    file
						    "-Goverlap=scalexy -Gnodesep=0.6 -Gstart=0.5" )))
	((eq renderer 'circo)
	 (sb-ext:run-program "/usr/bin/circo" (list "-T" "svg" "-O" file)))
	((eq renderer 'sfdp)
	 (sb-ext:run-program "/usr/bin/sfdp" (list "-T" "svg" "-O" file
						   "-Goverlap=scalexy -Gnodesep=0.6 -Gstart=0.5")))
	((eq renderer 'twopi)
	 (sb-ext:run-program "/usr/bin/twopi" (list "-T" "svg" "-O" file "-Goverlap=scalexy")))))

;; RANDOM
