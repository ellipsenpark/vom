(in-package :vom)

;; REBALANCE = naive edge rebalancing ...
;; not super elegant so far ...
;; rebalance sketch:
;; 0.) rebalance so that total number of points is 100 (see above)
;; 1.) define a factor that sets the ratio of the maximum balanced points
;;     (max bal points are the max number of points an edge can have in the case of total
;;      equality, that is, i.e., in the case of 2 edges, 50, or in the case of 4 edges, 25 etc)
;; 2.) then, split the edges into two list: A - above or on threshold B - below threshold
;; 3.) now, one by one, remove a point from the edges in list A (if possible, that is, edge doesn't fall below threshold)
;; 4.) and add it to the first edge in list B until the edge is on the threshold
;; 5.) remove that edge from list B and add to A
;; 6.) repeat 2.) to 5.) until list B is empty
;; 7.) return A
(defun rebalance (child-list &key (blur 0.0))
  ;; first, rebalance so the sum of probabilities is 100
  (let* ((total-prob (loop for choice in child-list summing (car choice)))
         (diff (- 100 total-prob))
         (cur-idx 0))
    (if (> diff 0) ;; there's something to distribute
        (loop repeat diff
              do (incf (car (nth cur-idx child-list)))
              do (setf cur-idx (mod (incf cur-idx) (length child-list))))        
        (loop while (< diff 0)
              do  (if (> (car (nth cur-idx child-list)) 0)                      
                      (progn (incf diff)
                             (decf (car (nth cur-idx child-list))))
                      (progn (decf diff)
                             (incf (car (nth cur-idx child-list)))))
              do (setf cur-idx (mod (incf cur-idx) (length child-list))))))
  (blur child-list blur))

(defun rebalance-float (cl-float &key (blur 0.0))
  ;; first, rebalance so the sum of probabilities is 100
  (let* ((child-list (mapcar #'(lambda (c) (list (floor (* 100 (car c))) (cadr c))) cl-float)))    
    (mapcar #'(lambda (c) (list (/ (car c) 100) (cadr c))) (rebalance child-list :blur blur))))

;; BLUR - even out probabilities
(defun blur (child-list blur)
  ;; otherwise the algorithm won't terminate ...
  (if (> blur 1.0) (setf blur 1.0))
  (if (< blur 0.0) (setf blur 0.0))
  (if (> blur 0.0)
      (let* ((min-points (floor (* (/ 100 (length child-list)) blur)))
             (below (remove-if-not #'(lambda (tr) (< (car tr) min-points)) child-list))
             (above (remove-if-not #'(lambda (tr) (> (car tr) min-points)) child-list))
             (cur-idx 0))
        (loop while below
              do (loop while (< (caar below) min-points)
                       do (if (> (car (nth cur-idx above)) min-points)                            
                              (progn (incf (caar below))
                                     (decf (car (nth cur-idx above)))))
                       do (setf cur-idx (mod (incf cur-idx) (length above))))
              do (push (car below) above)
              do (setf below (cdr below)))))
  child-list)

(defun blur-float (cl-float blur)
  ;; first, rebalance so the sum of probabilities is 100
  (let* ((child-list (mapcar #'(lambda (c) (list (floor (* 100 (car c))) (cadr c))) cl-float)))    
    (mapcar #'(lambda (c) (list (/ (car c) 100) (cadr c))) (blur child-list blur))))

(defmethod blur-pfa ((p naive-pfa) blur &key)
  (loop for key being the hash-keys of (children p)
        do (setf (gethash key (children p)) (blur (gethash key (children p)) blur))))

(defmethod blur-pfa ((p adj-list-pfa) blur &key)
  (loop for key being the hash-keys of (children p)
        do (setf (gethash key (children p)) (blur-float (gethash key (children p)) blur))))

;; SHARPEN - increment strongest edge ...
(defun sharpen (child-list sharpen)
  (let ((edges-sorted (sort child-list #'(lambda (a b) (> (car a) (car b))))))
    (unless (>= (caar edges-sorted) 100) ;; nothing to do in that case ...
      (let* ((smaller-edges (cdr edges-sorted))
             (available-points (- 100 (caar edges-sorted)))
             (points-to-distribute (floor (* available-points sharpen)))
             (cur-idx 0))
        (setf (caar edges-sorted) (+ (caar edges-sorted) points-to-distribute))
        (loop while (> points-to-distribute 0)
              when (> (car (nth cur-idx smaller-edges)) 0)
              do (decf (car (nth cur-idx smaller-edges)))
              and do (decf points-to-distribute)
              do (setf cur-idx (mod (incf cur-idx) (length smaller-edges)))))))
  child-list)

(defun sharpen-float (cl-float sharpen)
  ;; first, rebalance so the sum of probabilities is 100
  (let* ((child-list (mapcar #'(lambda (c) (list (floor (* 100 (car c))) (cadr c))) cl-float)))    
    (mapcar #'(lambda (c) (list (/ (car c) 100) (cadr c))) (sharpen child-list sharpen))))

(defmethod sharpen-pfa ((p naive-pfa) sharpen &key)
  (loop for key being the hash-keys of (children p)
        do (setf (gethash key (children p)) (sharpen (gethash key (children p)) sharpen))))

(defmethod sharpen-pfa ((p adj-list-pfa) sharpen &key)
  (loop for key being the hash-keys of (children p)
        do (setf (gethash key (children p)) (sharpen-float (gethash key (children p)) sharpen))))

(defun get-history-transitions (pfa)
  (loop for (a b) on (history pfa) while b
        nconc (remove-duplicates (get-all-transitions-between pfa a b))))

;; ENOURAGE - make current behaviour more likely
(defun encourage-pfa (pfa sharpen)  
  (loop for trans in (get-history-transitions pfa) do
        (let ((enc-edge
                (if (typep pfa 'naive-pfa)
                    (copy-list (get-transition pfa (car trans) (caadr trans)))
                    (copy-list (get-transition pfa (car trans) (cadr trans))))))          
          (if (typep pfa 'adj-list-pfa) (setf (car enc-edge) (ceiling (* 100 (car enc-edge)))))          
          (unless (>= (car enc-edge) 100) ;; nothing to do in that case ...            
            (let* ((other-edges
                     (if (typep pfa 'naive-pfa)
                         (remove enc-edge (copy-list (gethash (car trans) (children pfa))) :test 'dest-equals)
                         (mapcar #'(lambda (c) (list (floor (* 100 (car c))) (cadr c)))
                                 (remove enc-edge (copy-list (gethash (car trans) (children pfa))) :test 'dest-equals))))
                   (available-points (- 100 (car enc-edge)))
                   (points-to-distribute (floor (* available-points sharpen)))
                   (cur-idx 0))
              (setf (car enc-edge) (+ (car enc-edge) points-to-distribute))
              (loop while (> points-to-distribute 0)
                    when (> (car (nth cur-idx other-edges)) 0)
                    do (decf (car (nth cur-idx other-edges)))
                    and do (decf points-to-distribute)
                    do (setf cur-idx (mod (incf cur-idx) (length other-edges))))
              (setf (gethash (car trans) (children pfa))
                    (if (typep pfa 'naive-pfa)
                        (nconc (list enc-edge) other-edges)
                        (mapcar #'(lambda (c) (list (/ (car c) 100) (cadr c))) (nconc (list enc-edge) other-edges)))))))))

;; DISCOURAGE - make current behaviour less likely
(defun discourage-pfa (pfa sharpen)  
  (loop for trans in (get-history-transitions pfa) do
        (let ((disc-edge
                (if (typep pfa 'naive-pfa)
                    (copy-list (get-transition pfa (car trans) (caadr trans)))
                    (copy-list (get-transition pfa (car trans) (cadr trans))))))          
          (if (typep pfa 'adj-list-pfa) (setf (car disc-edge) (ceiling (* 100 (car disc-edge)))))          
          (unless (<= (car disc-edge) 0) ;; nothing to do in that case ...            
            (let* ((other-edges
                     (if (typep pfa 'naive-pfa)
                         (remove disc-edge (copy-list (gethash (car trans) (children pfa))) :test 'dest-equals)
                         (mapcar #'(lambda (c) (list (floor (* 100 (car c))) (cadr c)))
                                 (remove disc-edge (copy-list (gethash (car trans) (children pfa))) :test 'dest-equals))))
                   (available-points (car disc-edge))
                   (points-to-distribute (floor (* available-points sharpen)))
                   (cur-idx 0))
              ;;(format t "~D ~D ~D~%" other-edges (car trans) (caadr trans))
              (setf (car disc-edge) (- (car disc-edge) points-to-distribute))
              (when other-edges
                (loop while (> points-to-distribute 0)
                     when (< (car (nth cur-idx other-edges)) 100)
                     do (incf (car (nth cur-idx other-edges)))
                     and do (decf points-to-distribute)
                     do (setf cur-idx (mod (incf cur-idx) (length other-edges))))
               (setf (gethash (car trans) (children pfa))
                     (if (typep pfa 'naive-pfa)
                         (nconc (list disc-edge) other-edges)
                         (mapcar #'(lambda (c) (list (/ (car c) 100) (cadr c))) (nconc (list disc-edge) other-edges))))))))))

