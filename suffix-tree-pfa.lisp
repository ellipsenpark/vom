;; suffix tree PFAs combine a regular pfa representation with a suffix tree
;; (achieving some mixed suffix/prefix representation)
;; to allow for easier inference from incomplete rules ...
(in-package :vom)

(defclass st-pfa-state (pfa-state)
  ((suffix-tree-children
    :accessor st-pfa-state-st-children
    :initform (make-hash-table))
   (rule-state :accessor st-pfa-rule-state :initarg :rule :initform nil)))

(defclass st-pfa (pfa)
  ((root :accessor st-pfa-st-root :initform nil)))

(defmethod st-pfa-get-states-with-symbol ((s st-pfa) symbol &key)
  (labels ((collect-states (root symbol states)             
             (if (equal (hash-table-size (st-pfa-state-st-children root)) 0)
                 (if (member symbol (pfa-state-label root))
                     (nconc states (list (pfa-state-label root)))
                     states)
                 (if (member symbol (pfa-state-label root))
                     (nconc states (list (pfa-state-label root))
                            (loop for ch being the hash-values of (st-pfa-state-st-children root)
                                  nconc (collect-states ch symbol states)))
                     (nconc states 
                            (loop for ch being the hash-values of (st-pfa-state-st-children root)
                                  nconc (collect-states ch symbol states)))))))
    (collect-states (st-pfa-st-root s) symbol (list)))) 


(defmethod initialize-instance :after ((p st-pfa) &key)
  (let ((root-node (make-instance 'st-pfa-state)))
    (setf (pfa-state-label root-node) '())
    (setf (gethash '() (pfa-states p)) root-node)
    (setf (st-pfa-st-root p) root-node)))

(defmethod st-pfa-add-state ((p st-pfa) label &key (overwrite nil) (rule nil))
  (if (or overwrite (not (pfa-get-state p label)))
    (setf (gethash label (pfa-states p))
	  (make-instance 'st-pfa-state :label label :rule rule))
    (pfa-get-state p label)))

;; need to add prefix- and suffix states ... 
(defmethod st-pfa-add-st-state ((p st-pfa) label &key (overwrite nil))
  ;; add to "regular" pfa  
  (st-pfa-add-state p label :overwrite overwrite :rule t)
  ;; extend SUFFIX tree according to label ... 
  (let ((label-rev (reverse label)))
     (labels ((find-leaf-and-add (cur-node rev-label n-label)	       
		(when rev-label		   		 
		  (let ((cur-child (gethash (car rev-label)
					    (st-pfa-state-st-children cur-node))))
		    (push (car rev-label) n-label)
		    (when (not cur-child)
		      (format t "extended suffix tree by state ~D~%" n-label)
		      (setf (gethash (car rev-label)
				     (st-pfa-state-st-children cur-node))
			    (st-pfa-add-state p n-label)))
		    (find-leaf-and-add (gethash (car rev-label)
						(st-pfa-state-st-children cur-node))
				       (cdr rev-label) n-label)))))
       (find-leaf-and-add (st-pfa-st-root p) label-rev (list)))))

;; find the longest currents state corresponding to the suffix
(defmethod st-pfa-find-longest-suffix-state ((p st-pfa) suffix &key)
  (let ((rev-suffix (reverse suffix)))
    (labels ((trace-path (pstate rsuf)
	       (if (and rsuf
			(gethash (car rsuf) (st-pfa-state-st-children pstate)))
		   (trace-path (gethash (car rsuf)
					(st-pfa-state-st-children pstate))
			       (cdr rsuf))
		   pstate)))
      (trace-path (st-pfa-st-root p) rev-suffix))))

(defun infer-st-pfa (&rest rules)
  (infer-st-pfa-list rules))

;; add a state to the st-pfa by finding necessary prefixes AND suffixes ...
(defmethod st-pfa-add-prefix-suffix ((p st-pfa) label)
  (let ((prefix (list (car label)))
	(suffix (cdr label)))	    
    ;; determine prefix states we need to add ... 
    (loop while (> (length suffix) 1)
       do (let* ((intermediate (append prefix (list (car suffix)))))
	    (when (not (pfa-get-state p intermediate))
	      (format t "inferred prefix state: ~D~%" intermediate)
	      (st-pfa-add-st-state p intermediate)) 
	    (setq prefix intermediate)
	    (setq suffix (cdr suffix))))
    (st-pfa-add-st-state p label)))

(defun infer-st-pfa-list (rules)
  "infer a suffix tree pfa from a (possibly incomplete) user-provided scaffold" 
  ;; construct pfa
  (let ((new-pfa (make-instance 'st-pfa))
	(alphabet '()))      
    (loop for rule in rules
          do (push (car rule) alphabet)
          do (st-pfa-add-prefix-suffix new-pfa (car rule)))
    ;; prune alphabet
    (setf alphabet (remove-duplicates (alexandria:flatten alphabet)))
    (setf (alphabet new-pfa) alphabet)
    (loop for rule in rules
          do (let* ((src-state (car rule))
		    (dest-state (pfa-state-label (st-pfa-find-longest-suffix-state new-pfa (append src-state (list (nth 1 rule)))))))		
	       (pfa-add-transition new-pfa src-state dest-state (nth 1 rule) :prob (nth 2 rule))))
    ;; check the inferred or extended states that don't have any
    ;; transitions yet ...    
    (loop for state being the hash-values of (pfa-states new-pfa)	 
          do (when (> 1 (hash-table-count (pfa-state-transitions state)))
	       (loop for symbol in alphabet
	             do (let* ((last-sym (alexandria::lastcar (pfa-state-label state)))
			       (first-order-state (gethash (list last-sym) (pfa-states new-pfa))))                          
		          (when (and first-order-state (gethash symbol (pfa-state-next-sym-prob first-order-state)))
                            (let ((dest-state (pfa-state-label (st-pfa-find-longest-suffix-state new-pfa (append (pfa-state-label state) (list symbol))))))		
			      (pfa-add-transition new-pfa (pfa-state-label state) dest-state symbol :prob (gethash symbol (pfa-state-next-sym-prob first-order-state)))))))))
    ;; return pfa ... 
    new-pfa))

(defmethod st-pfa-inflate-state ((p st-pfa) label)
  (st-pfa-add-prefix-suffix p label)      
  ;; update states with the new transitions ...
  (loop for state being the hash-values of (pfa-states p)	 
	do (loop for symbol in (alphabet p)
		 ;; first order state and dest state might be the same, but don't have to be ... 
		 do (let* ((dest-state (st-pfa-find-longest-suffix-state p (append (pfa-state-label state) (list symbol))))
			   (first-order-state (gethash (last (pfa-state-label state)) (pfa-states p)))
			   (prob (if (gethash symbol (pfa-state-next-sym-prob state))
				     (gethash symbol (pfa-state-next-sym-prob state))
				     (gethash symbol (pfa-state-next-sym-prob first-order-state)))))
		      (when (and dest-state prob)
			(pfa-add-transition p (pfa-state-label state) (pfa-state-label dest-state) symbol :prob prob))))))

;; extend an st-pfa with a new symbol ...
;; source-state must be present or inflateable (that is, taken from the history)
(defmethod st-pfa-extend-with-symbol ((p st-pfa) source-state new-symbol exit &key (prob-ratio 0.3))
  ;; after this, we can be sure the new source state exists ...
  ;; ensure this first 
  (unless (pfa-get-state p source-state)
    (st-pfa-inflate-state p source-state))
  ;; now extend the alphabet ...
  (push new-symbol (alphabet p))
  ;; now, calculate the new state's share of probablity ...
  (let* ((state (pfa-get-state p source-state))
	 (prob-allot (loop for sym being the hash-keys of (pfa-state-next-sym-prob state)				    
			summing (let* ((old-prob (gethash sym (pfa-state-next-sym-prob state)))
				       (prob-share (* old-prob prob-ratio)))
				  (setf (gethash sym (pfa-state-next-sym-prob state)) (- old-prob prob-share))				 
				  prob-share)))
	 (symbol-label (list new-symbol))
	 (dest-state (pfa-state-label (st-pfa-find-longest-suffix-state p exit))))
    (st-pfa-add-prefix-suffix p symbol-label)
    (pfa-add-transition p source-state (list new-symbol) new-symbol :prob prob-allot)
    ;; re-build the choice list,
    ;; as all probs have been manipulated ...    
    ;; create exit transition ...
    (pfa-add-transition p symbol-label dest-state (car dest-state) :prob 1.0)))

;; growth method as formerly done in the megra model ... 
(defmethod grow-st-pfa ((p st-pfa) pick dist exit &key (prob-ratio 0.5))
  (let* ((pick-rev (- (length (pfa-history p)) pick))
	 (pick-dist (- pick-rev dist))
	 (pick-exit (- (length (pfa-history p)) exit))
	 (template (nth pick-rev (pfa-history p)))
	 (new-symbol (gensym))) ;; better naming ?    
    (st-pfa-extend-with-symbol p
			       (subseq (pfa-history p) pick-dist pick-rev)
			       new-symbol
			       (list (nth pick-exit (pfa-history p)))
			       :prob-ratio prob-ratio) ;; this might be nicer ! 
    ;; return new symbol and the symbol this was spawned from ...
    (list template new-symbol)))

;; this is dirty, it'd be way better to implement the learning algorithm directly
;; on the suffix-tree-pfa model ...
(defun pfa->st-pfa (p)
  (change-class p 'st-pfa)
  (let ((root-node (make-instance 'st-pfa-state)))
    (setf (pfa-state-label root-node) '())
    (setf (gethash '() (pfa-states p)) root-node)
    (setf (st-pfa-st-root p) root-node))
  (loop for state being the hash-values of (pfa-states p)
     do (progn
	  (change-class state 'st-pfa-state)
	  (setf (st-pfa-state-st-children state) (make-hash-table))))
  (loop for state being the hash-values of (pfa-states p)
     do (st-pfa-add-prefix-suffix p (pfa-state-label state))))
