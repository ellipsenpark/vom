(in-package :vom)

(defclass adj-list-pfa ()
  ((pst-root :accessor pst-root :initarg :pst-root
             :initform (make-pst-node :label nil
                                      :child-prob (make-hash-table :test 'equal)
                                      :children (make-hash-table :test 'equal)))
   (alphabet :accessor alphabet :initform '() :initarg :alphabet)
   (current-state :accessor current-state :initform nil) 
   (parents :accessor parents :initform (make-hash-table :test 'equal))
   (childern :accessor children :initform (make-hash-table :test 'equal))
   (history :accessor history :initform '()) ;; trace is essential part of the model
   (history-length :accessor history-length :initform 9)))

;; for now, build choice list on the fly ...
;; returns next node, and where it's coming from ...
(defmethod next-transition ((p adj-list-pfa) &key)
  ;; first, find highest-order parent from history ...
  ;; need to copy here, unfortunately ...
  ;; build choice list ... this is pretty inefficient, but this version of the
  ;; pfa is built for clarity more than efficency ...
  (let* ((parent (current-state p))
         (choice-list (loop for choice in (gethash parent (children p)) 
                            nconc (make-list (round (* 100 (coerce (car choice) 'float))) :initial-element (cadr choice))))
         (next-state (nth (random (length choice-list)) choice-list)))
    ;; update the history .. why does it go haywire if I use nconc here ???
    (setf (history p) (append (history p) (list (alexandria::lastcar next-state))))
    ;; truncate history if necessary ...
    (when (> (length (history p)) (history-length p)) (setf (history p) (cdr (history p))))
    ;; set new current node ...
    (setf (current-state p) next-state)
    ;; return transition with parent and current ...
    (make-query-result :last-state parent :current-state (current-state p) :symbol (alexandria::lastcar (current-state p)))))

(defmethod add-child ((p adj-list-pfa) src dest prob &key)
  (add-to-nodup (children p) src (cons prob (list dest)) :test 'dest-equals))

(defmethod add-parent ((p adj-list-pfa) dest src &key)  
  (add-to-nodup (parents p) dest src :test 'equal))

;; INSERT
;; label always a list
(defmethod insert-state ((p adj-list-pfa) label &key)
  (setf (alphabet p) (remove-duplicates (nconc (alphabet p) label)))
  (setf (gethash label (parents p)) '())
  (setf (gethash label (children p)) '())
  (unless (current-state p)
    (setf (current-state p) label)
    (setf (history p) (nconc (history p) label))))

(defmethod get-all-transitions-between ((p adj-list-pfa) src-sym dest-sym &key)
  (loop for src in (get-suffix-symbol-states (pst-root p) src-sym)
        nconc (loop for dest in (get-suffix-symbol-states (pst-root p) dest-sym)
                    when (has-transition p src dest)
                    collect (list src dest))))

(defmethod insert-transition ((p adj-list-pfa) src dest prob &key (rebalance nil))  
  (add-child p src dest prob)
  (add-parent p dest src)
  (if rebalance (rebalance-state p src))
  (cons src (alexandria::lastcar dest)))

;; add between two symbols
(defmethod insert-transition-everywhere ((p adj-list-pfa) suffix dest prob &key (rebalance nil))
  (loop for src in (get-suffix-symbol-states (pst-root p) suffix)
        collect (insert-transition p src dest prob :rebalance rebalance)))

(defmethod rebalance-state ((p adj-list-pfa) source &key)
  (setf (gethash source (children p)) (rebalance-float (gethash source (children p)) :blur 0.35)))

(defmethod rebalance-states ((p adj-list-pfa) states &key)
  (loop for state in states do (rebalance-state p state)))

(defmethod rebalance-all-states ((p adj-list-pfa) &key)
  (loop for states in (alexandria::hash-table-keys (children p))
        do (rebalance-state p states)))

(defmethod rebalance-pfa ((p adj-list-pfa) &key)
  (rebalance-all-states p))

(defun pst-nostar->adj-list-pfa (extended-pst-root alphabet)
  "convert pst to adj-list-pfa if pst doesn't have 'star' property ... "
  ;; leaves and all the prefixes are the states of the pfa,
  ;; meaning all the states of the pst, basically ... 
  (let ((new-pfa (make-instance 'adj-list-pfa :pst-root extended-pst-root)))
    ;; add states
    (labels ((copy-state (node)
	       (insert-state new-pfa (pst-node-label node))
	       (loop for child being the hash-values of (pst-node-children node)
		     do (copy-state child))))
      ;; copy them over ... 
      (copy-state extended-pst-root))
    ;; add transitions
    (loop for state being the hash-keys of (children new-pfa)
          do (let ((pst-state (find-longest-suffix-state extended-pst-root state)))
	       (loop for symbol in alphabet
	             do (let ((longest-suffix-pst-state
			        (find-longest-suffix-state
			         extended-pst-root (append state (list symbol))))
                              (tprob (gethash symbol (pst-node-child-prob pst-state))))
                          (when (> tprob 0.0)
                            (insert-transition new-pfa
					       state
					       (pst-node-label longest-suffix-pst-state)					     
					       tprob))))))
    new-pfa))

(defun learn-adj-list-pfa (alphabet bound epsilon n sample-string)
  "learn an adj-list-pfa from a given sample"
  (let ((new-pst (learn-pst alphabet bound epsilon n sample-string)))
    (pst-nostar->adj-list-pfa (extend-pst new-pst (collect-pst-leaves new-pst) alphabet) alphabet)))

;; caould be done more elegantly with loop ...
(defmethod adj-list-pfa-add-prefix-suffix ((p adj-list-pfa) label)
  (let ((prefix (list (car label)))
	(suffix (cdr label))
        (added nil))
    ;; insert base state
    (insert-state p label)
    ;; determine prefix states we need to add ... 
    (loop while (> (length suffix) 1)
          do (let* ((intermediate (append prefix (list (car suffix)))))               
               (when (not (gethash intermediate (children p)))  
	         (insert-state p intermediate)
                 (push intermediate added)) 
	       (setq prefix intermediate)
	       (setq suffix (cdr suffix))))
    added))

(defun find-prob (list label)
  (loop for item in list
        when (equal (last (cadr item)) label)
        return (car item)))

(defun infer-adj-list-pfa (&rest rules)
  (infer-adj-list-pfa-list rules))

;; impl transition randomization ! 
(defmethod randomize-edges ((p adj-list-pfa) chance &key prop)
  (let ((rnd-edges (delete nil (loop for child being the hash-keys of (children p) using (hash-value v)
                                     append (loop for dest in (alphabet p)
                                                  unless (member (list 0 (list dest)) v :test 'dest-equals)
                                                  collect (when (< (random 100) chance) (list (copy-list child) (list dest))))))))
    (loop for edge in rnd-edges do (insert-transition p (car edge) (cadr edge)
                                                      (if prop
                                                          (floor (/ (random prop) 100.0))
                                                          0.0)))
    rnd-edges))

;; insert node into an existing pfa ...
;; check for correctness, rebalance eventually !
;; if length is one, just add transition ??
;; return added transitions !!!
(defmethod insert-rule ((p adj-list-pfa) rule &key)  
  (let ((added-transitions '()))
    (let ((added-symbol (loop for state in (adj-list-pfa-add-prefix-suffix p (car rule))
                              nconc (add-node (pst-root p) state))))
      (setf added-symbol (nconc (add-node (pst-root p) (car rule)) added-symbol))     
      (loop for n in added-symbol
            when (not (gethash n (children p)))
            do (insert-state p n)))
    (let* ((src (car rule))
	   (dest (find-longest-suffix-label (pst-root p) (append src (list (nth 1 rule))))))
      (push (list src dest) added-transitions)
      (insert-transition p src dest (nth 2 rule)))
    (loop for state being the hash-keys of (children p) using (hash-value ch)
          when (not (equal state (car rule))) ;; only those that weren't handled by explicit rules (and thus, are in the hand of the user)
          do (loop for symbol in (alphabet p)
	           do (let* ((last-sym (last state)) ;; this needs better naming !!!
                             (last-sym-ch (gethash last-sym (children p)))
                             (prob (find-prob last-sym-ch (list symbol))))                        
		        (when (and last-sym-ch prob)
                          ;;(format t "~D ~D ~D ~D~%" state last-sym last-sym-ch (list symbol))
                          (let ((dest-state (find-longest-suffix-label (pst-root p) (append state (list symbol)))))		
                            (push (list state dest-state) added-transitions)
                            (insert-transition p state dest-state prob))))))
    added-transitions))

;; here, the suffix tree isn't really used to infer the probabilities, but only for lookup ... 
(defun infer-adj-list-pfa-list (rules)
  "infer an adj list pfa from a (possibly incomplete) user-provided scaffold"  
  ;; construct pfa
  (let ((p (make-instance 'adj-list-pfa :alphabet (remove-duplicates (loop for rule in rules append (car rule))))))
    ;; add implicit states that model repetitions etc ...
    (loop for rule in rules do (adj-list-pfa-add-prefix-suffix p (car rule)))
    ;; infer a suffix tree for lookup ...
    (let ((added-symbol (loop for state being the hash-keys of (children p) append (add-node (pst-root p) state))))
      ;;(format t "ADDED DURING PST EXT: ~D~%" added-symbol)
      (loop for n in added-symbol
            when (not (gethash n (children p)))
            do (insert-state p n)))
    ;; add explicitly defined transitions ...
    (loop for rule in rules
          do (let* ((src (car rule))
		    (dest (find-longest-suffix-label (pst-root p) (append src (list (nth 1 rule))))))		
	       (insert-transition p src dest (nth 2 rule))))
    ;; add implicitly defined transitions ...
    (loop for state being the hash-keys of (children p) using (hash-value ch)
          when (eql 0 (length ch)) ;; only those that weren't handled by explicit rules (and thus, are in the hand of the user)
          do (loop for symbol in (alphabet p)
	           do (let* ((last-sym (last state)) ;; this needs better naming !!!
                             (last-sym-ch (gethash last-sym (children p)))
                             (prob (find-prob last-sym-ch (list symbol))))                        
		        (when (and last-sym-ch prob)
                          ;;(format t "~D ~D ~D ~D~%" state last-sym last-sym-ch (list symbol))
                          (let ((dest-state (find-longest-suffix-label (pst-root p) (append state (list symbol)))))		
			    (insert-transition p state dest-state prob))))))
    ;; return p
    p))

(defmethod remove-transition ((p adj-list-pfa) source destination &key (rebalance nil) (rebuild-tree nil))  
  (setf (gethash source (children p)) (delete (list 0 destination) (gethash source (children p)) :test 'dest-equals))
  (setf (gethash destination (parents p)) (delete source (gethash destination (parents p)) :test 'equal))
  (if (and rebalance (gethash source (children p))) ;; only if there's something to do ...
      (setf (gethash source (children p)) (rebalance-float (gethash source (children p))))))

(defmethod has-transition ((p adj-list-pfa) source dest &key (test 'dest-equals))
  (member (list 0 dest) (gethash source (children p)) :test 'dest-equals))

(defmethod get-transition ((p adj-list-pfa) src dest &key)
  (car (member (list 0 dest) (gethash src (children p)) :test 'dest-equals)))

;; this might lead to dead states, but that's ok so far ...
;; good enough for now ...
(defmethod remove-transition-everywhere ((p adj-list-pfa) suffix destination &key (rebalance nil))
  (let* ((to-remove (loop for source in (vom::get-suffix-symbol-states (pst-root p) suffix) nconc
                         (loop for dest in (vom::get-suffix-symbol-states (pst-root p) destination)            
                               when (has-transition p source dest)               
                               collect (list source dest)))))
    ;; remove the transitions ...
    (loop for rem in to-remove nconc (remove-transition p (car rem) (cadr rem)))    
    ;; return removed states and list of removed transitions ...
    to-remove))

(defmethod remove-outgoing-transitions ((p adj-list-pfa) state &key)
  (loop for child in (gethash state (children p))
        do (remove-transition p state (cadr child))
        collect (list (list state (cadr child)) (car child)) ;; src - dest - old prob
        finally (setf (gethash state (children p)) nil)))

(defmethod remove-incoming-transitions ((p adj-list-pfa) state &key)
  (loop for parent in (gethash state (parents p))
        do (remove-transition p parent state :rebalance nil)        
        collect (list parent state)))

(defmethod purge-state ((p adj-list-pfa) id &key)  
  (let ((removed-transitions (nconc (mapcar #'car (remove-outgoing-transitions p id)) (remove-incoming-transitions p id))))    
    (when (equal (current-state p) id)
      (setf (current-state p) (last (history p)))) 
    (remhash id (parents p))
    (remhash id (children p))
    (make-growth-result :added-symbol nil
                        :template-symbol nil
                        :removed-states (list id)
                        :added-transitions nil
                        :removed-transitions (delete nil removed-transitions))))

(defmethod rebuild-pst ((p adj-list-pfa) removed-states)
  (let ((new-pst (make-pst-node :label nil :children (make-hash-table :test 'equal) :child-prob (make-hash-table :test 'equal))))
    (loop for state in (remove-all removed-states (alexandria::hash-table-keys (children p)))
          do (add-node new-pst state))
    (setf (pst-root p) new-pst)))
  
(defmethod remove-symbol ((p adj-list-pfa) removed-symbol &key (rebalance nil))
  (let* ((states-to-remove (get-states-with-symbol (pst-root p) removed-symbol))         
         (results (loop for state in states-to-remove
                        collect (let* ((removed-out (remove-outgoing-transitions p state))
                                       (removed-in (remove-incoming-transitions p state))         
                                       (removed-all (append removed-in (mapcar #'car removed-out)))
                                       ;; avoid loops being re-added
                                       (edges-to-add (loop for ei in removed-in
                                                           append (loop for eo in removed-out
                                                                        when (and (not (equal (car ei) state)) (not (equal (cadar eo) state)))
                                                                        collect (list (list (car ei) (cadar eo)) (cadr eo))))))
                                  ;;(format t "STATE: ~D removed in: ~D ~%" state removed-in)
                                  ;;(format t "STATE: ~D removed out: ~D ~%" state removed-out)
                                  ;;(format t "STATE: ~D to-add : ~D ~%" state edges-to-add)
                                  (when (equal (current-state p) state)
                                    (setf (current-state p) (list (car (alphabet p))))
                                    (setf (history p) (append (history p) (list (car (alphabet p))))))    
                                  (loop for a in edges-to-add do
                                        ;;(format t "STATE: ~D add trans: ~D ~D ~D ~%" state (caar a) (cadar a) (cadr a))
                                        (insert-transition p (caar a) (cadar a) (cadr a) :rebalance rebalance))
                                  (remhash state (children p))
                                  (remhash state (parents p)) ;; no need to convert to list here, because there shouldn't be any list keys anyway ...    
                                  ;; communicate what has been done to the outside world ...
                                  (list edges-to-add removed-all)))))
    ;; rebuild pst with states to keep
    (rebuild-pst p states-to-remove)
    ;; i hope the garbage collection deletes the old one ???
    ;; delete from path and history
    (setf (history p) (delete removed-symbol (history p)))
    (setf (alphabet p) (delete removed-symbol (alphabet p)))
    ;; security measure
    (when (member removed-symbol (current-state p))
      (setf (current-state p) (list (car (alphabet p))))
      (setf (history p) (append (history p) (list (car (alphabet p))))))    
    (let* ((removed-edges (delete-duplicates (mapcar #'(lambda (tr) (cons (car tr) (alexandria::lastcar (cadr tr))))
                                   (loop for res in results append (cadr res))) :test 'equal))
           (added-edges (remove-all removed-edges
                                    (delete-duplicates (mapcar #'(lambda (tr) (cons (car tr) (alexandria::lastcar (cadr tr))))
                                                               (loop for res in results append (mapcar 'car (car res)))) :test 'equal))
                        
                        ))
      (make-growth-result :added-symbol nil
                          :template-symbol nil
                          :removed-states states-to-remove                          
                          :added-transitions added-edges
                          :removed-transitions removed-edges))))

(defmethod is-abandoned ((p adj-list-pfa) id &key)  
  (not (gethash id (parents p)))) 

;; naive edge rebalancing ...
;; not super elegant so far ...
;; rebalance sketch:
;; 0.) rebalance so that total number of points is 100 (see above)
;; 1.) define a factor that sets the ratio of the maximum balanced points
;;     (max bal points are the max number of points an edge can have in the case of total
;;      equality, that is, i.e., in the case of 2 edges, 50, or in the case of 4 edges, 25 etc)
;; 2.) then, split the edges into two list: A - above or on threshold B - below threshold
;; 3.) now, one by one, remove a point from the edges in list A (if possible, that is, edge doesn't fall below threshold)
;; 4.) and add it to the first edge in list B until the edge is on the threshold
;; 5.) remove that edge from list B and add to A
;; 6.) repeat 2.) to 5.) until list B is empty
;; 7.) return A


(defmethod post-growth ((p adj-list-pfa) new-id states higher-order rnd)  
  (let ((new-edges '()))    
    (unless (> rnd 0)
      (rebalance-states p states))    
    (when (> higher-order 1)      
      (setf new-edges (nconc new-edges (insert-rule p (list (last (history p) higher-order) (car new-id) 1.0)))))
    (when (> rnd 0)
      (setf new-edges (nconc new-edges (randomize-edges p rnd)))
      (rebalance-all-states p))
    ;; transform to transition format 
    (delete-duplicates (mapcar #'(lambda (tr) (cons (car tr) (alexandria::lastcar (cadr tr)))) new-edges) :test 'equal)))

(defmethod pad-path ((p adj-list-pfa) &key)
  (if (< (length (history p)) (history-length p))	
      (loop repeat (- (history-length p) (length (history p)))
	    do (push (car (history p)) (history p)))))

(defmethod sweep-abandoned-states ((p adj-list-pfa) removed-transitions &key keep)
  (delete-duplicates (loop for tr in removed-transitions 
                           when (and (is-abandoned p (car tr))
                                     (not (member (car tr) keep :test 'equal)))
                           do (purge-state p (car tr))
                           and collect (car tr)
                           when (and (is-abandoned p (cadr tr))
                                     (not (member (cadr tr) keep :test 'equal)))
                           do (purge-state p (cadr tr))
                           and collect (cadr tr)) :test 'equal))

(defmethod grow-old ((p adj-list-pfa) &key (rnd 0) (higher-order 0))
  (pad-path p) ;; make sure path is at the right length
  (let* ((source-id (car (history p)))	 
	 (dest-id (car (last (history p))))
	 (node-id (nth (random (history-length p)) (history p))) 	 
	 (new-id (list (gensym))))
    (insert-state p new-id)
    (add-node (pst-root p) new-id)    
    (make-growth-result :added-symbol (car new-id)
                        :template-symbol node-id
                        :added-transitions (delete-duplicates                                            
                                            (nconc
                                             (list (insert-transition p new-id (list source-id) (+ 0.05 (/ (random 20) 100)) :rebalance nil)
                                                   (insert-transition p new-id (list dest-id) (+ 0.05 (/ (random 20) 100)) :rebalance nil))
                                             (insert-transition-everywhere p source-id new-id (+ 0.05 (/ (random 20) 100)) :rebalance nil)
                                             (insert-transition-everywhere p dest-id new-id (+ 0.05 (/ (random 20) 100)) :rebalance nil)
                                             (post-growth p new-id (list new-id (list source-id) (list dest-id)) higher-order rnd))
                                            :test 'equal))))

(defmethod grow-flower ((p adj-list-pfa) &key (rnd 0) (higher-order 0))
  (pad-path p) ;; make sure path is at the right length
  (let* ((source-id (car (history p)))	 	 
	 (new-id (list (gensym))))
    (insert-state p new-id)
    (add-node (pst-root p) new-id)    
    (make-growth-result :added-symbol (car new-id)
                        :template-symbol source-id
                        :added-transitions (delete-duplicates                                            
                                            (nconc
                                             (list (insert-transition p new-id (list source-id) (+ 0.05 (/ (random 20) 100)) :rebalance nil))
                                             (insert-transition-everywhere p source-id new-id (+ 0.05 (/ (random 20) 100)) :rebalance nil)
                                             (post-growth p new-id (list new-id (list source-id)) higher-order rnd))
                                            :test 'equal))))

(defmethod grow-triloop ((p adj-list-pfa) &key (rnd 0) (higher-order 0))
  (pad-path p)
  (let* ((source-id (car (last (history p))))	 
	 (dest-id (car (last (history p) 2)))
	 (node-id (nth (random (history-length p)) (history p)))
	 (new-id (list (gensym))))
    (insert-state p new-id)
    (add-node (pst-root p) new-id)
    (let* ((removed-transitions (remove-transition-everywhere p source-id dest-id :rebalance nil))
           ;; now let's look for eventual abandoned states that can be removed ...
           ;; only remove those that won't be connected again ... 
           (removed-states (sweep-abandoned-states p removed-transitions :keep (list (list source-id) (list dest-id)))))
      ;; rebuild pst before inserting, so the old states
      ;; won't be found ...
      (rebuild-pst p removed-states)
      (make-growth-result :added-symbol (car new-id)
                          :template-symbol node-id
                          :removed-states removed-states
                          :removed-transitions (delete-duplicates (mapcar #'(lambda (tr) (cons (car tr) (alexandria::lastcar (cadr tr)))) removed-transitions) :test 'equal)
                          :added-transitions (delete-duplicates
                                                     (nconc  
                                                      (list (insert-transition p new-id (list dest-id) 0.5 :rebalance nil))
                                                      (insert-transition-everywhere p source-id new-id 1.0 :rebalance nil)
                                                      (post-growth p new-id (list new-id (list source-id) (list dest-id)) higher-order rnd))
                                                     :test 'equal)))))

(defmethod grow-quadloop ((p adj-list-pfa) &key (rnd 0) (higher-order 0))
  (pad-path p)
  (let* ((source-id (car (last (history p))))
	 (dest-id (car (last (history p) 3)))
	 (node-id (nth (random (history-length p)) (history p))) ;; pick a node id 
	 (new-id (list (gensym))))    
    ;; insert the new node
    (insert-state p new-id)
    (add-node (pst-root p) new-id)
    (let* ((removed-transitions (remove-transition-everywhere p source-id dest-id :rebalance nil))
           ;; now let's look for eventual abandoned states that can be removed ...
           ;; only remove those that won't be connected again ... 
           (removed-states (sweep-abandoned-states p removed-transitions :keep (list (list source-id) (list dest-id)))))
      ;; rebuild pst before inserting, so the old states
      ;; won't be found ...
      (rebuild-pst p removed-states)
      (make-growth-result :added-symbol (car new-id)
                          :template-symbol node-id
                          :removed-states removed-states
                          :removed-transitions (delete-duplicates (mapcar #'(lambda (tr) (cons (car tr) (alexandria::lastcar (cadr tr)))) removed-transitions) :test 'equal)
                          :added-transitions (delete-duplicates
                                                     (nconc  
                                                      (list (insert-transition p new-id (list dest-id) 0.5 :rebalance nil))
                                                      (insert-transition-everywhere p source-id new-id 1.0 :rebalance nil)
                                                      (post-growth p new-id (list new-id (list source-id) (list dest-id)) higher-order rnd))
                                                     :test 'equal)))))

(defmethod grow-loop ((p adj-list-pfa) &key (rnd 0) (higher-order 0))
  (pad-path p)
  (let* ((dest-id (car (last (history p))))	 
	 (source-id (car (last (history p) 2)))
	 (node-id (nth (random (history-length p)) (history p))) ;; pick a node id 	 
	 (new-id (list (gensym))))    
    ;; insert the new node
    (insert-state p new-id)
    (add-node (pst-root p) new-id)
    (let* ((removed-transitions (remove-transition-everywhere p source-id dest-id :rebalance nil))
           ;; now let's look for eventual abandoned states that can be removed ...
           ;; only remove those that won't be connected again ... 
           (removed-states (sweep-abandoned-states p removed-transitions :keep (list (list source-id) (list dest-id)))))
      ;; rebuild pst before inserting, so the old states
      ;; won't be found ...
      (rebuild-pst p removed-states)
      (make-growth-result :added-symbol (car new-id)
                          :template-symbol node-id
                          :removed-states removed-states
                          :removed-transitions (delete-duplicates (mapcar #'(lambda (tr) (cons (car tr) (alexandria::lastcar (cadr tr)))) removed-transitions) :test 'equal)
                          :added-transitions (delete nil
                                                     (nconc  
                                                      (list (insert-transition p new-id (list dest-id) 0.5 :rebalance nil))
                                                      (insert-transition-everywhere p source-id new-id 1.0 :rebalance nil)
                                                      (post-growth p new-id (list new-id (list source-id) (list dest-id)) higher-order rnd)))))))

;; GRAPHVIZ VISUALIZATION
(defmethod adj-list-pfa->dot ((g adj-list-pfa) &key (output nil))
  (format output "digraph{~%")
  (loop for label being the hash-keys of (children g) using (hash-value chs)
        do (progn
             (format output "\"~{~a~^, ~}\"[label=\"~{~a~^, ~}\"]~%" label label)             
             (loop for ch in chs
                   do (format output "\"~{~a~^, ~}\"->\"~{~a~^, ~}\"[weight=~a, penwidth=~a, rank=same, arrowsize=~a]~%"
	                      label
                              (cadr ch)
	                      (coerce (car ch) 'float)
	                      (coerce (car ch) 'float)
	                      (coerce (car ch) 'float)
                              ;;(alexandria::lastcar (cadr ch))
                              ))))
  (format output "~%}"))

(defmethod prune-pfa ((g adj-list-pfa) &key exclude node-id)
  (let* ((reduced-history (remove-all exclude (history g)))
	 (prune-id (if node-id node-id (car (last reduced-history)))))    
    ;; otherwise the graph can't be pruned further ... 
    (when prune-id
      (remove-symbol g prune-id :rebalance t))))

(defun adj-list-pfa->svg (graph file &key (renderer 'dot))
  (with-open-file (out-stream file :direction :output :if-exists :supersede)
    (adj-list-pfa->dot graph :output out-stream))
  (cond ((eq renderer 'dot)
	 (sb-ext:run-program "/usr/bin/dot" (list "-T" "svg" "-O" file "-Gnslimit" "-Gnslimit1")))
	((eq renderer 'neato)
	 (sb-ext:run-program "/usr/bin/neato" (list "-T" "svg" "-O" file "-Goverlap=scalexy -Gnodesep=0.6 -Gstart=0.5" )))
	((eq renderer 'circo)
	 (sb-ext:run-program "/usr/bin/circo" (list "-T" "svg" "-O" file)))
	((eq renderer 'sfdp)
	 (sb-ext:run-program "/usr/bin/sfdp" (list "-T" "svg" "-O" file "-Goverlap=scalexy -Gnodesep=0.6 -Gstart=0.5")))
	((eq renderer 'twopi)
	 (sb-ext:run-program "/usr/bin/twopi" (list "-T" "svg" "-O" file "-Goverlap=scalexy")))))
