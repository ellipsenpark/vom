(in-package :vom)

;; growth operation ...

(defmethod add-higher-order-edge ((p naive-pfa) order new-node-id &key)
  (insert-edge p (copy-list (last (history p) order)) new-node-id 100)
  (cons (copy-list (last (history p) order)) new-node-id))

(defmethod rebalance-nodes ((p naive-pfa) nodes &key)
  (loop for node in nodes do (rebalance-node p node)))

(defmethod rebalance-all-nodes ((p naive-pfa) &key)
  (loop for node in (alexandria::hash-table-keys (children p))
        do (rebalance-node p node)))

(defmethod rebalance-pfa ((p naive-pfa) &key)
  (rebalance-all-nodes p))

(defmethod get-all-transitions-between ((p naive-pfa) src-sym dest-sym &key)
  (loop for par in (gethash dest-sym (parents p))
        when (equal (alexandria::lastcar par) src-sym)
        collect (list par (list dest-sym))))

;; add random edges
(defmethod randomize-edges ((p naive-pfa) chance &key prop) 
  (let ((rnd-edges (delete nil (loop for child being the hash-keys of (children p) using (hash-value v)
                                     append (loop for dest in (alphabet p)
                                                  unless (member (cons 0 dest) v :test 'dest-equals)
                                                  collect (when (< (random 100) chance) (cons (copy-list child) dest)))))))
    (loop for edge in rnd-edges do (insert-edge p (car edge) (cdr edge)
                                                (if prop
                                                    (random prop)
                                                    0)))
    rnd-edges))

(defmethod post-growth ((p naive-pfa) new-id nodes higher-order rnd)
  (let ((new-edges '()))
    (unless (> rnd 0)
      (rebalance-nodes p nodes))
    (when (> higher-order 1)      
      (push (add-higher-order-edge p higher-order new-id) new-edges))
    (when (> rnd 0)
      (setf new-edges (nconc new-edges (randomize-edges p rnd)))
      (rebalance-all-nodes p))
    new-edges))

(defmethod pad-path ((p naive-pfa) &key)
  (if (< (length (history p)) (history-length p))	
      (loop repeat (- (history-length p) (length (history p)))
	    do (push (car (history p)) (history p)))))

(defmethod grow-old ((p naive-pfa) &key (rnd 0) higher-order)
  (pad-path p) ;; make sure path is at the right length
  (let* ((source-id (car (history p)))	 
	 (dest-id (car (last (history p))))
	 (node-id (nth (random (history-length p)) (history p))) 	 
	 (new-id (gensym)))
    (insert-node p new-id)    
    (insert-edge p (list new-id) source-id (+ 5 (random 20)))
    (insert-edge p (list source-id) new-id (+ 5 (random 20)))
    (insert-edge p (list new-id) dest-id (+ 5 (random 20)))
    (insert-edge p (list dest-id) new-id (+ 5 (random 20)))
    (make-growth-result :added-symbol new-id
                        :template-symbol node-id
                        :added-transitions (delete nil
                                                   (nconc
                                                    (list (cons (list new-id) source-id)
                                                          (cons (list source-id) new-id)
                                                          (cons (list new-id) dest-id)
                                                          (cons (list dest-id) new-id))
                                                    (post-growth p new-id (list (list new-id) (list source-id) (list dest-id)) higher-order rnd))))))

(defmethod grow-flower ((p naive-pfa) &key (rnd 0) higher-order)
  (pad-path p) ;; make sure path is at the right length
  (let* ((source-id (car (history p)))	 	 
	 (new-id (gensym)))
    (insert-node p new-id)    
    (insert-edge p (list new-id) source-id (+ 5 (random 20)))
    (insert-edge p (list source-id) new-id (+ 5 (random 20)))   
    (make-growth-result :added-symbol new-id
                        :template-symbol source-id
                        :added-transitions (delete nil
                                                   (nconc
                                                    (list (cons (list new-id) source-id)
                                                          (cons (list source-id) new-id))
                                                    (post-growth p new-id (list (list new-id) (list source-id)) higher-order rnd))))))

(defmethod grow-triloop ((p naive-pfa) &key rnd higher-order)
  (pad-path p)
  (let* ((source-id (car (last (history p))))	 
	 (dest-id (car (last (history p) 2)))
	 (node-id (nth (random (history-length p)) (history p)))
	 (new-id (gensym)))
    (insert-node p new-id)
    (insert-edge p (list new-id) dest-id 100)
    (insert-edge p (list source-id) new-id 100)
    (remove-edge p (list source-id) dest-id :rebalance nil)
    (make-growth-result :added-symbol new-id
                        :template-symbol node-id
                        :removed-transitions (list (cons (list source-id) dest-id))
                        :added-transitions (delete nil
                                                   (nconc
                                                    (list (cons (list new-id) dest-id)
                                                          (cons (list source-id) new-id))
                                                    (post-growth p new-id (list (list new-id) (list source-id) (list dest-id)) higher-order rnd))))))

(defmethod grow-quadloop ((p naive-pfa) &key rnd higher-order)
  (pad-path p)
  (let* ((source-id (car (last (history p))))
	 (dest-id (car (last (history p) 3)))
	 (node-id (nth (random (history-length p)) (history p))) ;; pick a node id 
	 (new-id (gensym)))    
    ;; insert the new node
    (insert-node p new-id)
    (insert-edge p (list new-id) dest-id 100)
    (insert-edge p (list source-id) new-id 100)
    (remove-edge p (list source-id) dest-id)
    (make-growth-result :added-symbol new-id
                        :template-symbol node-id
                        :removed-transitions (list (cons (list source-id) dest-id))
                        :added-transitions (delete nil
                                                   (nconc
                                                    (list (cons (list new-id) dest-id)
                                                          (cons (list source-id) new-id))
                                                    (post-growth p new-id (list (list new-id) (list source-id) (list dest-id)) higher-order rnd))))))

(defmethod grow-loop ((p naive-pfa) &key rnd higher-order)
  (pad-path p)
  (let* ((dest-id (car (last (history p))))	 
	 (source-id (car (last (history p) 2)))
	 (node-id (nth (random (history-length p)) (history p))) ;; pick a node id 	 
	 (new-id (gensym)))    
    ;; insert the new node
    (insert-node p new-id)
    (insert-edge p (list new-id) dest-id 100)
    (insert-edge p (list source-id) new-id 100)
    (remove-edge p (list source-id) dest-id)
    (make-growth-result :added-symbol new-id
                        :template-symbol node-id
                        :removed-transitions (list (cons (list source-id) dest-id))
                        :added-transitions (delete nil
                                                   (nconc
                                                    (list (cons (list new-id) dest-id)
                                                          (cons (list source-id) new-id))
                                                    (post-growth p new-id (list (list new-id) (list source-id) (list dest-id)) higher-order rnd))))))

(defmethod prune ((p naive-pfa) &key exclude node-id)
  (let* ((exclude-with-current (append exclude (list (current-node p))))
	 (reduced-path (remove-all exclude-with-current (history p)))
	 (prune-id (if node-id node-id (car (last reduced-path)))) ;; avoid pruning current node
	 (replacement-id (alexandria::random-elt (remove (current-node p) (alphabet p)))))
    (setf (history p) (remove prune-id (history p)))
    (when prune-id
      ;; otherwise the graph can't be pruned further ... 
      (when (eql (current-node p) prune-id)
	(unless (history p)
	  (setf (history p) (list replacement-id)))	
	(setf (current-node p) replacement-id))
      (remove-node p prune-id :rebalance t))))

(defun infer-naive-pfa (&rest rules)
  (infer-naive-pfa-list rules))

(defun infer-naive-pfa-list (rules)
  (let ((p (make-instance 'naive-pfa :alphabet (remove-duplicates (alexandria::flatten (mapcar #'car rules))))))
    (setf (history p) (list (caaar rules)))
    (loop for rule in rules
          do (insert-edge p (nth 0 rule) (nth 1 rule) (if (< (nth 2 rule) 1) (floor (* (nth 2 rule) 100)) (nth 2 rule))))
    p))

