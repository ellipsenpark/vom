;;;; vom - variable-order markov graphs
;;;; part of megra
(asdf:defsystem #:vom
  :serial t
  :author "Niklas Reppel"
  :description "vom - variable-order markov chains and graphs"
  :license "GPLv3"
  :depends-on (#:alexandria)
  :components ((:file "package")
               (:file "common")
	       (:file "pst")
               (:file "pfa")
               (:file "pfa-adj-list")               
               (:file "suffix-tree-pfa")
               (:file "naive-pfa")
               (:file "naive-pfa-operations")
               (:file "pfa-edge-operations")
               (:file "transfer-state")
	       (:file "lz78")))
